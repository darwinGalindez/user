package com.user.model.dto.user.testdatabuilder;

import com.user.model.dto.user.UserDto;

public class UserDtoTestDataBuilder {

    private String firstName;
    private String lastName;
    private String email;
    private String password;

    public UserDtoTestDataBuilder() {
        firstName = "Pepito";
        lastName = "Perez";
        email = "test@gmail.com";
        password = "1234";
    }

    public UserDto build() {
        return new UserDto(firstName, lastName, email, password);
    }
}
