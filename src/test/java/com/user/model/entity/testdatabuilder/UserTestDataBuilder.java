package com.user.model.entity.testdatabuilder;

import com.user.model.entity.User;

public class UserTestDataBuilder {

    private Integer id;

    private String firstName;
    private String lastName;
    private String email;
    private String password;

    public UserTestDataBuilder() {
        id = 1;
        firstName = "Pepito";
        lastName = "Perez";
        email = "test@gmail.com";
        password = "1234";
    }

    public User build() {
        return new User(id, firstName, lastName, email, password);
    }
}
