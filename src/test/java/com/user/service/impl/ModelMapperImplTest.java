package com.user.service.impl;

import com.user.model.dto.user.testdatabuilder.UserDtoTestDataBuilder;
import com.user.model.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ModelMapperImplTest {

    private ModelMapperImpl modelMapperImpl;

    @BeforeEach
    void setUp() {
        modelMapperImpl = new ModelMapperImpl();
    }

    @Test
    void convertDtoToEntity() {

        var userDto = new UserDtoTestDataBuilder().build();
        var user = modelMapperImpl.convertDtoToEntity(userDto, User.class);

        Assertions.assertEquals("User", user.getClass().getSimpleName());
    }
}
