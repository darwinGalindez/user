package com.user.service.impl;

import com.user.exception.UserException;
import com.user.model.dto.user.UserDto;
import com.user.model.dto.user.testdatabuilder.UserDtoTestDataBuilder;
import com.user.model.entity.User;
import com.user.model.entity.testdatabuilder.UserTestDataBuilder;
import com.user.repository.UserRepository;
import com.user.service.ModelMapperService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public class UserServiceImplTest {

    private static final int QUANTITY_CALLS = 1;
    private static final int CODE_SUCCESS = 1;
    private static final int CODE_ERROR = 0;
    private static final int PAGE = 1;
    private static final int SIZE = 10;
    private static final String MESSAGE_USER_EXIST = "El usuario con el email %s ya se encuentra registrado";

    private UserRepository userRepository;
    private ModelMapperService<UserDto, User> modelMapperService;
    private UserServiceImpl userServiceImpl;

    @BeforeEach
    void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        modelMapperService = Mockito.mock(ModelMapperService.class);
        userServiceImpl = new UserServiceImpl(userRepository, modelMapperService);
    }

    @Test
    void registerTest() {
        var userDto = new UserDtoTestDataBuilder().build();
        var user = new UserTestDataBuilder().build();

        Mockito.when(userRepository.findByEmail(userDto.getEmail()))
                .thenReturn(null);
        Mockito.when(modelMapperService.convertDtoToEntity(Mockito.any(), Mockito.any()))
                .thenReturn(user);
        Mockito.when(userRepository.save(user))
                .thenReturn(user);

        var responseService = userServiceImpl.register(userDto);

        Mockito.verify(userRepository, Mockito.times(QUANTITY_CALLS)).findByEmail(userDto.getEmail());
        Mockito.verify(modelMapperService, Mockito.times(QUANTITY_CALLS)).convertDtoToEntity(Mockito.any(), Mockito.any());
        Mockito.verify(userRepository, Mockito.times(QUANTITY_CALLS)).save(Mockito.any());
        Assertions.assertEquals(CODE_SUCCESS, responseService.getCode());
    }

    @Test
    void registerWithError() {
        var userDto = new UserDtoTestDataBuilder().build();
        var user = new UserTestDataBuilder().build();

        Mockito.when(userRepository.findByEmail(userDto.getEmail()))
                .thenReturn(user);

        try {
            userServiceImpl.register(userDto);
        } catch (UserException e) {
            Assertions.assertEquals(e.getCode(), CODE_ERROR);
            Assertions.assertEquals(e.getMessage(), String.format(MESSAGE_USER_EXIST, userDto.getEmail()));
        }

        Mockito.verify(userRepository, Mockito.times(QUANTITY_CALLS)).findByEmail(userDto.getEmail());
    }

    @Test
    void findAllUsersTest() {
        var pageRequest = PageRequest.of(PAGE, SIZE);
        var users = List.of(new UserTestDataBuilder().build());
        var usersPaginated = new PageImpl<>(users);

        Mockito.when(userRepository.findAll(pageRequest))
                .thenReturn(usersPaginated);

        var responseService = userServiceImpl.findAllUsers(PAGE, SIZE);

        Mockito.verify(userRepository, Mockito.times(QUANTITY_CALLS)).findAll(pageRequest);
        Assertions.assertEquals(CODE_SUCCESS, responseService.getCode());
        Assertions.assertEquals(PAGE, responseService.getParameters().size());
    }

    @Test
    void findUserByMailTest() {
        var user = new UserTestDataBuilder().build();

        Mockito.when(userRepository.findByEmail(user.getEmail()))
                .thenReturn(user);

        var responseService = userServiceImpl.findUserByMail(user.getEmail());

        Mockito.verify(userRepository, Mockito.times(QUANTITY_CALLS)).findByEmail(user.getEmail());
        Assertions.assertEquals(CODE_SUCCESS, responseService.getCode());
        Assertions.assertEquals(PAGE, responseService.getParameters().size());
    }
}
