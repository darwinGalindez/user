package com.user.exception;

import com.user.model.dto.response.ResponseGeneralDto;
import com.user.model.entity.User;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestControllerAdvice
public class ControllerExceptions {

    private static final int CODE_UNHANDLED_ERROR = 0;
    private static final String MESSAGE_UNHANDLED_ERROR = "Error en el servicio. Por favor vuelvalo a intentar";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(value = UserException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseGeneralDto<User> userException(UserException e) {

        logger.error(e.toString());
        return new ResponseGeneralDto<>(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(value = Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseGeneralDto<User> processUnhandledError(Exception e) {

        logger.error(e.toString());
        return new ResponseGeneralDto<>(CODE_UNHANDLED_ERROR, MESSAGE_UNHANDLED_ERROR);
    }
}
