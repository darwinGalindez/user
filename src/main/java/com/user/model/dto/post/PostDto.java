package com.user.model.dto.post;

import com.user.model.entity.User;
import lombok.Getter;
import lombok.Setter;

import java.sql.Time;
import java.sql.Timestamp;

@Getter
@Setter
public class PostDto {

    private int id;
    private Integer views;
    private String title;
    private String body;
    private Timestamp createdAt;
    private Time updatedAt;
    private int userId;
}
