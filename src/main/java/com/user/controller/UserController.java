package com.user.controller;

import com.user.model.dto.response.ResponseGeneralDto;
import com.user.model.dto.user.UserDto;
import com.user.model.entity.User;
import com.user.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * USER CONTROLLER
 *
 * */
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

	private final UserService userService;

    /**
     *
     * Register a user
     *
     * **/
	@PostMapping(
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	ResponseGeneralDto<User> register(@RequestBody UserDto userDto, HttpServletRequest request) {
		var responseService = userService.register(userDto);
		setRequestUri(request, responseService);

		return responseService;
	}

    /**
     *
     * Get all users (paginated)
     *
     * **/
	@GetMapping(
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	ResponseGeneralDto<User> findAllUsers(@RequestParam int page, @RequestParam int size, HttpServletRequest request) {
		var responseService = userService.findAllUsers(page, size);
		setRequestUri(request, responseService);

		return responseService;
	}

	/**
	 * Find a user by email
	 */
	@GetMapping(
			path = "/by-mail",
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	ResponseGeneralDto<User> findUserByMail(@RequestParam String email, HttpServletRequest request) {
		var responseService = userService.findUserByMail(email);
		setRequestUri(request, responseService);

		return responseService;
	}

	private void setRequestUri(HttpServletRequest request, ResponseGeneralDto<User> responseGeneralDto) {
		responseGeneralDto.setUrl(request.getRequestURI());
	}
}
