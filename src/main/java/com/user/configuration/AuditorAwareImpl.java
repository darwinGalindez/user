package com.user.configuration;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    private static final String NAME_AUDITOR = "Darwin";

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(NAME_AUDITOR);
    }
}
