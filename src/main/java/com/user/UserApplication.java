package com.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.user.service"})
@ComponentScan(basePackages = {"com.user.service.impl"})
@ComponentScan(basePackages = {"com.user.controller"})
@ComponentScan(basePackages = {"com.user.model"})
@ComponentScan(basePackages = {"com.user.repository"})

@SpringBootApplication
@EnableDiscoveryClient
public class UserApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserApplication.class, args);
	}

}
