package com.user.service.impl;

import com.user.exception.UserException;
import com.user.model.dto.PageableDto;
import com.user.model.dto.response.ResponseGeneralDto;
import com.user.model.dto.user.UserDto;
import com.user.service.ModelMapperService;
import com.user.service.UserService;
import com.user.model.entity.User;
import com.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 *  CLASS USER SERVICE
 *
 * ****/
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private static final int CODE_SUCCESS = 1;
    private static final int CODE_ERROR = 0;
    private static final String MESSAGE_REGISTER_SUCCESS = "Se registro correctamente el usuario";
    private static final String MESSAGE_SUCCESS = "Se realizo la operación exitosamente";
    private static final String MESSAGE_USER_EXIST = "El usuario con el email %s ya se encuentra registrado";

    @Autowired
    private UserRepository userRepository;

    private ModelMapperService<UserDto, User> modelMapperService;

    @Override
    public ResponseGeneralDto<User> register(UserDto userDto) {

        var userByEmail = getUserByEmail(userDto.getEmail());

        if(userByEmail != null) {
            throw new UserException(CODE_ERROR, String.format(MESSAGE_USER_EXIST, userDto.getEmail()));
        }

        var user = modelMapperService.convertDtoToEntity(userDto, User.class);
        userRepository.save(user);

        return new ResponseGeneralDto<>(CODE_SUCCESS, MESSAGE_REGISTER_SUCCESS);
    }

    @Override
    public ResponseGeneralDto<User> findAllUsers(int page, int size) {

        var pageRequest = PageRequest.of(page, size);
        var usersPaginated = userRepository.findAll(pageRequest);
        var pageable = new PageableDto(usersPaginated.getPageable(), usersPaginated.getTotalPages(), ((int) usersPaginated.getTotalElements()));

        return new ResponseGeneralDto<>(CODE_SUCCESS, MESSAGE_SUCCESS, usersPaginated.toList(), pageable);
    }

    @Override
    public ResponseGeneralDto<User> findUserByMail(String email) {

        var user = getUserByEmail(email);

        return new ResponseGeneralDto<>(CODE_SUCCESS, MESSAGE_SUCCESS, List.of(user));
    }

    private User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }
}
