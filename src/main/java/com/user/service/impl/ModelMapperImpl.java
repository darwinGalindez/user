package com.user.service.impl;

import com.user.service.ModelMapperService;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

@Service
public class ModelMapperImpl implements ModelMapperService {

    @Override
    public Object convertDtoToEntity(Object dto, Class entityClass) {
        var modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT);

        return modelMapper.map(dto, entityClass);
    }
}
