package com.user.service;

import com.user.model.dto.response.ResponseGeneralDto;
import com.user.model.dto.user.UserDto;
import com.user.model.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    /**
     *
     * Register a user
     *
     * **/
    // void register(UserDto userDto);
    ResponseGeneralDto<User> register(UserDto userDto);

    /**
     *
     * Get all users (paginated)
     *
     * **/
    ResponseGeneralDto<User> findAllUsers(int page, int size);

    /**
     * Find a user by email
     */
    ResponseGeneralDto<User> findUserByMail(String email);

}
